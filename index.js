/*	while loop
	- takes in an expression/condition, if the condition evaluates to true,
	the statements inside the code block will be executed.
	Syntax:
		while (expression/condition) {
			statement/s;
		}
*/

let count = 5;
while (count >= 0) {
	console.log("While: " + count);
	count--;
}

let countAgain = 0;
console.log("\nStart loop 0-9:")
while (countAgain < 10) {
	console.log("While: " + countAgain);
	countAgain++;
}

/*	do while loop
	- works a lot like the while loop. But the do-while loop guarantees that the code will be executed
	atleast once.
	Syntax:
		do {
			statement/s
		} while (expression/condition)
*/

let number = Number(prompt("Give me a number"));
do {
	console.log("Do While: " + number);
	number += 1;
} while (number < 10)

/*	for loop
	- more flexible than while and do-while loops. It consist of three parts:
	 1. Initialization - a value that will track the progression of the loop.
	 2. Expression/dondition = determines whether the loop will run one more time.
	 3. finalExpression = indicates how to advance the loop.
	Syntax:
		for (initialization, expression/condition; finalExpression) {
			statement/s;
		}
*/

for (let count3 = 0; count3 <= 3; count3++) {
	console.log(count3);
}


let myString = "alex";

console.log("Count string length:", myString.length);
console.log(typeof myString.length);

console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);

console.log("Word Loop");
for (let x = 0; x < myString.length; x++) {
	console.log(myString[x]);
}


console.log("Replace vowels with a number:")
// let myName = "AlEx";
let myName = "Marco Macdon";
for (let i = 0; i < myName.length; i++) {
	// console.log(myName[i].toLowerCase());

	if (
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u"
	) {
		console.log(3);
	} else {
		console.log(myName[i]);
	}
}

/* CONTINUE and BREAK statements
	continue - a statement that allows the code to go to the next iteration of the loop
		without finishing the execution of all statements in a code block.
	break - a statement that is used to terminate the current loop once a match has been found.
*/

for (let count4 = 0; count4 <= 20; count4++) {
	if (count4 % 2 === 0) {
		continue;
	}

	console.log("Continue and Break: " + count4);

	if (count4 > 10) {
		break;
	}
}


console.log("\nIterating the length of string:");

let name = "alexandro";

for (let i = 0; i < name.length; i++) {
	console.log(name[i]);

	if (name[i].toLowerCase() === "a") {
		console.log("Continue to the next iteration");
		continue;
	}

	if (name[i] === "d") {
		break;
	}
}